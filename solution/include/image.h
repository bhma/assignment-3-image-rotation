#ifndef INC_3_IMAGE_ROTATION_IMAGE_H
#define INC_3_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image image_create(const uint32_t width, const uint32_t height);

void image_destroy(struct image *image);

#endif
