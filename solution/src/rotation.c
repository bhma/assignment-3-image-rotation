#include "rotation.h"
#include "image.h"
#include <stdint.h>

struct image rotate(struct image const source) {
    struct image image = image_create(source.height, source.width);
    for (uint32_t i = 0; i < source.height; i++) {
        for (uint32_t j = 0; j < source.width; j++) {
            image.data[source.height * (j + 1) - i - 1] = source.data[source.width * i + j];
        }
    }
    return image;
}
