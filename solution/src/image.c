#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct image image_create(const uint32_t width, const uint32_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void image_destroy(struct image *image) {
    if (image->data == NULL) {
        return;
    }
    free(image->data);
}
