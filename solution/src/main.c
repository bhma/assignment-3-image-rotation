#include "bmp.h"
#include "image.h"
#include "rotation.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "This program accepts only three arguments: <source-image>, <transformed-image> and <angle>\n");
        return 1;
    }
    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        fprintf(stderr, "Cannot find input file\n");
        return 1;
    }

    struct image image = {0};
    if (from_bmp(in, &image) != READ_OK) {
        fprintf(stderr, "Error during reading input file\n");
        image_destroy(&image);
        fclose(in);
        return 1;
    }
    fclose(in);
    uint32_t angle = (atoi(argv[3]) + 360) % 360;
    if (angle != 0 && angle != 90 && angle != 180 && angle != 270) {
        image_destroy(&image);
        fprintf(stderr, "Invalid ange. Accepted only: 0, 90, -90, 180, -180, 270, -270\n");
        return 1;
    }
    uint32_t rotations = (4 - angle / 90) % 4;
    for (uint32_t i = 0; i < rotations; i++) {
        struct image rotated = rotate(image);
        image_destroy(&image);
        image = rotated;
    }

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        fprintf(stderr, "Cannot find output file\n");
        image_destroy(&image);
        return 1;
    }
    if (to_bmp(out, &image) != WRITE_OK) {
        fprintf(stderr, "Error during writing\n");
        image_destroy(&image);
        fclose(out);
        return 1;
    }

    image_destroy(&image);
    fclose(out);
    fprintf(stdout, "Image has been rotated\n");
    return 0;
}
